# Information
This project takes in tracking data from VR and-
- visually simulates how participants acted
- creates training files that consist of transitions done by participants
- uses training files to create models explaining the tracking data

# Running the project
The executable is `Build\Build.exe`. It can be run with the following command-line arguments-
To visual investigate the tracking file-
```
Build.exe <tracking_file_location> <stimulus_number>
```
To create a training file
```
Build.exe  <tracking_file_location>
```
# Development

## Unity3D
- The entire folder can be opened in Unity as a Unity project.
- The primary aim of the Unity project is to viusalize tracking data and generate training data (inferences on where the user was looking).

## Python
- Sripts are in the folder titled `Training`.
- `numpy`, `matplotlib` and `hmmlearn` are prequisites.
- The training files are `TrainingData-temporal.txt` and `TrainingData-transitional.txt`. The temporal file has an entry for region inference for each data point in the tracking file. The transitional file only has entries for data points where there was a transition from one region to another.
- The HMM script uses `TrainingData-transitional.txt` and MM script uses `TrainingData-temporal.txt`.
- The folder Training\AngularInference` includes scripts that work on tracking data for direct inferences on angular movements of the head.