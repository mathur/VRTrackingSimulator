﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CylindricalCollider : MonoBehaviour {
    public GameObject Textbox; //To show region number
    public GameObject Master; //Where OnlineMover and data writer scripts are attached
    public static int regionId=-1; //Name of the region we are looking at 

    private void FixedUpdate() //May be executed many times in a frame
    {
        RaycastHit objectHit;
        Vector3 fwd = transform.TransformDirection(Vector3.up);
        Debug.DrawRay(transform.position, fwd * 12, Color.green);
        if (Physics.Raycast(transform.position, fwd, out objectHit, 12))
        {
            Textbox.GetComponent<Text>().text = "Looking at Region " + objectHit.transform.name;
            regionId = int.Parse(objectHit.transform.name);
        }
        else
            Debug.LogError("No region intersection encountered!"); //Not looking at any region
    }
}
