﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TrainingDataWriter : MonoBehaviour
{
    private StreamWriter writer; //Writer of the result
    TrainingData prevData;

    public void Start()
    {
        
        if (GetComponent<OnlineMover>().mode == 1)
        {
            //Open a file for writing
            writer = new StreamWriter("TrackingData\\TrainingData-temporal-temp" + ".txt");
            writer.AutoFlush = false;
        }
    }
    public void WriteTrainingData(int inferredReg)
    {
        int targetRegion=-1;
        if(GetComponent<OnlineMover>().KeyedExperiment[GetComponent<OnlineMover>().RelevantTrackingData[0].stimulusNum].region1Colour == "Green") //Check which region is not green
        {
            targetRegion = GetComponent<OnlineMover>().KeyedExperiment[GetComponent<OnlineMover>().RelevantTrackingData[0].stimulusNum].regionID_2;
        }
        else
            targetRegion = GetComponent<OnlineMover>().KeyedExperiment[GetComponent<OnlineMover>().RelevantTrackingData[0].stimulusNum].regionID_1;


        /*
        if (OnlineMover.firstInSeries) //First in series
        {
            prevData = new TrainingData(GetComponent<OnlineMover>().RelevantTrackingData[0], targetRegion, -1); //The inferred region is wrong
        }
        else
        {
            string dataToWrite;
            TrainingData data = new TrainingData(GetComponent<OnlineMover>().RelevantTrackingData[0], targetRegion, inferredReg);

            if (prevData != null)
            {
                prevData.lookingAtRegion = inferredReg; //Set it to the proper value
                dataToWrite = JsonUtility.ToJson(prevData) + "\n" + JsonUtility.ToJson(data); //Write previous and current value
            }
            else
            {
                dataToWrite = JsonUtility.ToJson(data); //Just write current value
            }
            Debug.Log(dataToWrite);
            
            writer.WriteLine(dataToWrite); //Write to file

            prevData = null; //Reset the class for previous data point
        }
        */

        TrainingData data = new TrainingData(GetComponent<OnlineMover>().RelevantTrackingData[0], targetRegion, inferredReg);
        string dataToWrite = JsonUtility.ToJson(data);
        writer.WriteLine(dataToWrite); //Write to file
    }


    void OnApplicationQuit()
    {
        if(GetComponent<OnlineMover>().mode == 1)
            writer.Close();
    }

}
