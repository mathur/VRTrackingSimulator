﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class OnlineMover : MonoBehaviour {

    public Transform Head; // Reference to the head transform
    public Transform Torso; // Reference to the torso transform

    List<PoseInfo> TrackingData; //List of all tracking data
    public List<PoseInfo> RelevantTrackingData; //Relevant data
    public Dictionary<int, SceneDescription> KeyedExperiment = new Dictionary<int, SceneDescription>(); //The experiment with stimulus number as key
    public int mode = 0; //0- Manual, 1-Automated

    //private float timeAtStart;

    private float heightOfUserAtStart=-1;
    private int sceneNum=-1;

    bool activeUpdate = false; //Used to slow down Update function

    //public static bool firstInSeries = true;
    private void Awake()
    {
        // Read the tracking file
        StreamReader reader;

        string trackingFile = "TrackingData\\S012.txt";
        int stimulusNum = 50;

        // Give a value to firstBlock depending on the argument passed 
        if (System.Environment.GetCommandLineArgs().Length == 3)
        {
            trackingFile = System.Environment.GetCommandLineArgs()[1];
            stimulusNum = int.Parse(System.Environment.GetCommandLineArgs()[2]);
            mode = 0;
        }
        else if (System.Environment.GetCommandLineArgs().Length == 2)
        {
            mode = 1;
            trackingFile = System.Environment.GetCommandLineArgs()[1];
            Debug.Log("Automated mode. All data for " + trackingFile + " shall be evaluated");
        }
        else
        {
            mode = 1;
            Debug.Log("Automated mode. All data shall be evaluated");
        }

        reader = new StreamReader(trackingFile);

        //Initialize lists
        TrackingData = new List<PoseInfo>();
        RelevantTrackingData = new List<PoseInfo>();


        string readData = reader.ReadLine(); // Read first line
        PoseInfo currPose;
        //Then, read in a loop
        while (readData != null)
        {
            currPose = JsonUtility.FromJson<PoseInfo>(readData);

            //If the trial matches the one required to be simulated
            if (currPose.trialNum == stimulusNum)
                RelevantTrackingData.Add(currPose);

            TrackingData.Add(currPose);
      
            readData = reader.ReadLine();
        }
        Debug.Log(TrackingData.Count + " track points read.");
        //Now, we read the experiment description
        reader = new StreamReader("config-mod.txt");
        readData = reader.ReadLine(); 

        while (readData != null) 
        {
            SceneDescription temp = JsonUtility.FromJson<SceneDescription>(readData);
            KeyedExperiment.Add(temp.stimulusNumber, temp);
            readData = reader.ReadLine();
        }
    }

    void Start ()
    {
        //timeAtStart = Time.time;
        if (mode == 0)
        {
            // Start InvokeRepeating
            InvokeRepeating("MoveTransformAndRemove", 1.0f, 0.2f);
        }
        else if (mode == 1)
        {
            RelevantTrackingData = new List<PoseInfo>(TrackingData);
        }
        else
            Debug.LogError("Unknown mode.");
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Time elapsed : " + (Time.time - timeAtStart).ToString() + " seconds.");
        if (mode == 1 && (RelevantTrackingData.Count > 0)) //If Automated mode & can execute
        {
            //If scene does not have same colourtargets
            //if (!KeyedExperiment[RelevantTrackingData[0].stimulusNum].twoTargets)
            //{
                MoveTransform(); //Move the transform according to stored pose
                activeUpdate = true;
            //}
            //else
            //{
                //Skip this stimulus as it has two targets
              //  Debug.Log("Skipping stimulus number " + RelevantTrackingData[0].stimulusNum);
              //  activeUpdate = false; //This is not to be written

            //}
            
        }
    }
    private void LateUpdate() //Executed after Update()
    {
        //Write the result
        if (mode == 1 && activeUpdate)
        {
            if (CylindricalCollider.regionId > 0) //Valid entry
            {
                GetComponent<TrainingDataWriter>().WriteTrainingData(CylindricalCollider.regionId);
            }
            else
            {
                Debug.LogError("Unknown regionId inferred");
            }
            
        }
        activeUpdate = false; //Reset active update
        if (RelevantTrackingData.Count > 0)
            RelevantTrackingData.RemoveAt(0); //Each frame we remove an element
        else
        {
            Debug.Log("Application quitting..");
            Application.Quit(); //Quit the application
        }
    }

    //This function is called in an InvokeRepeating manner
    private void MoveTransformAndRemove()
    {
        //Move transform according to the next track point 

        if (RelevantTrackingData.Count > 0) //If there are still things in the list
        {
            if (sceneNum == -1 || sceneNum != RelevantTrackingData[0].stimulusNum) //If sceneNUm not set or same as before
            {
                heightOfUserAtStart = RelevantTrackingData[0].headPos.y; //Reset initial height for offsetting
                sceneNum = RelevantTrackingData[0].stimulusNum;
            }

            Head.position = RelevantTrackingData[0].headPos - new Vector3(0, heightOfUserAtStart, 0); //Offset
            //Debug.Log("Head position is " + Head.position);
            Head.rotation = Quaternion.Euler(RelevantTrackingData[0].headRotEuler - new Vector3(0, 90, 0));
            //Torso.position = RelevantTrackingData[0].torsoPos;
            //Torso.rotation = Quaternion.Euler(RelevantTrackingData[0].torsoRotEuler);
            RelevantTrackingData.RemoveAt(0);
        }

    }
    
    //Called repeatedly to move transform to tracked pose
    private void MoveTransform()
    {
        //Move transform according to the next track point 
        
        if (RelevantTrackingData.Count > 0) //If there are still things in the list
        {
            if (sceneNum == -1 || sceneNum != RelevantTrackingData[0].stimulusNum) //If sceneNUm not set or same as before
            {
                heightOfUserAtStart = RelevantTrackingData[0].headPos.y; //Reset initial height for offsetting
                sceneNum = RelevantTrackingData[0].stimulusNum;
            }

            Head.position = RelevantTrackingData[0].headPos - new Vector3(0, heightOfUserAtStart, 0); //Offset
            //Debug.Log("Head position is " + Head.position);
            Head.rotation = Quaternion.Euler(RelevantTrackingData[0].headRotEuler - new Vector3(0,90,0));
            //Torso.position = RelevantTrackingData[0].torsoPos;
            //Torso.rotation = Quaternion.Euler(RelevantTrackingData[0].torsoRotEuler);
        }
        
    }
}
