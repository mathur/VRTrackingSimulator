﻿//This script infers region numbers incorrectly

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionInference : MonoBehaviour {

    //Set the ranges for polar and elevation angles
    private static float[] polarRanges = new float[] { -Mathf.PI / 2f, -Mathf.PI / 4f, 0, Mathf.PI / 4f, Mathf.PI / 2f };
    private static float[] elevationRanges = new float[] { 0f, Mathf.PI / 4f, Mathf.PI / 2f, Mathf.PI * 3f / 4f, Mathf.PI, Mathf.PI * 5f / 4f, Mathf.PI * 3f / 2f, Mathf.PI * 7f / 4f, Mathf.PI * 2f };

    public static int GetRegNum(Vector3 smallCubePos)
    {
        int regionCounter = 0; //Counts regions as objects are being filled in 

        Vector3 sphericalPos = SphericalFromCartesian(smallCubePos);
        Debug.Log("Spherical position of small cube is " + sphericalPos.ToString());
        for (int verCounter = 0; verCounter < (elevationRanges.Length - 1); verCounter++) //For all vertical regions
        {
            for (int horCounter = 0; horCounter < (polarRanges.Length - 1); horCounter++) //For all horizontal regions
            {
                regionCounter++; // Get the correct region number

                //The first if-else does sign switching required to maintain region id ordering
                if (elevationRanges[verCounter] < Mathf.PI / 2f || elevationRanges[verCounter] > Mathf.PI * 3f / 2f)
                {
                    if ((sphericalPos.y > polarRanges[horCounter] && sphericalPos.y < polarRanges[horCounter + 1]) &&
                         (sphericalPos.z > elevationRanges[verCounter] && sphericalPos.z < elevationRanges[verCounter + 1]))
                        return regionCounter;
        
                }

                else
                {
                    if ((sphericalPos.y > -polarRanges[horCounter] && sphericalPos.y < -polarRanges[horCounter + 1]) &&
                        (sphericalPos.z > elevationRanges[verCounter] && sphericalPos.z < elevationRanges[verCounter + 1]))
                        return regionCounter;

                }

            }
        }
        return -1; //Error, region not found
    }
    public static Vector3 SphericalFromCartesian(Vector3 cartesianCoordinate)
    {
        Vector3 spherical; //x=radius, y=polar, z=elevation

        if (cartesianCoordinate.x == 0f)
            cartesianCoordinate.x = Mathf.Epsilon;
        spherical.x = cartesianCoordinate.magnitude;

        spherical.y = Mathf.Atan(cartesianCoordinate.z / cartesianCoordinate.x);

        if (cartesianCoordinate.x < 0f)
            spherical.y += Mathf.PI;
        spherical.z = Mathf.Asin(cartesianCoordinate.y / spherical.x);

        return spherical;
    }
}
