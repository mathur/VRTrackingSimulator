﻿//This script infers region numbers incorrectly

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegionBorderCreator : MonoBehaviour {

    public GameObject Border; //Attached in inspector
    //Set the ranges for polar and elevation angles
    private static float[] polarRanges = new float[] { -Mathf.PI / 2f, -Mathf.PI / 4f, 0, Mathf.PI / 4f, Mathf.PI / 2f };
    private static float[] elevationRanges = new float[] { 0f, Mathf.PI / 4f, Mathf.PI / 2f, Mathf.PI * 3f / 4f, Mathf.PI, Mathf.PI * 5f / 4f, Mathf.PI * 3f / 2f, Mathf.PI * 7f / 4f, Mathf.PI * 2f };

    void Start()
    {
        //Place the borders
        PlaceBorders();
    }

    public void PlaceBorders()
    {
        int regionCounter = 0; //Counts regions as objects are being filled in 
        
        for (int verCounter = 0; verCounter < (elevationRanges.Length - 1); verCounter++) //For all vertical regions
        {
            for (int horCounter = 0; horCounter < (polarRanges.Length - 1); horCounter++) //For all horizontal regions
            {
                regionCounter++; // Get the correct region number

                //The configuration of the border being puut up
                float polarAngle, elevationAngle = (elevationRanges[verCounter] + elevationRanges[verCounter + 1]) / 2f, radius = 10f;

                Vector3 position;

                //To maintain region id ordering
                if (elevationRanges[verCounter] < Mathf.PI / 2f || elevationRanges[verCounter] > Mathf.PI * 3f / 2f)
                {
                    polarAngle = (polarRanges[horCounter] + polarRanges[horCounter + 1]) / 2f;
                }

                else
                {
                    polarAngle = (-polarRanges[horCounter] - polarRanges[horCounter + 1]) / 2f;
                }
                //Convert to cartesian
                SphericalToCartesian(radius, polarAngle, elevationAngle, out position);
                //Create G.O.
                GameObject go = (GameObject) Instantiate(Border, position, Quaternion.identity);
                go.transform.LookAt(Vector3.zero); //Look at the origin
                go.transform.name = regionCounter.ToString();
            }
        }
        Debug.Log("Borders placed!");
        
    }

    public static void SphericalToCartesian(float radius, float azimuth, float inclination, out Vector3 cartesianCoord)
    {
        float a = radius * Mathf.Cos(inclination);
        cartesianCoord = new Vector3(a * Mathf.Cos(azimuth), radius * Mathf.Sin(inclination), a * Mathf.Sin(azimuth));
    }

}
