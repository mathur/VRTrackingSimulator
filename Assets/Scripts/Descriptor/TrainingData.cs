﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TrainingData
{
    public string subjectID;

    public int stimulusNum;
    public int targetRegionNumber;

    public int lookingAtRegion; //Where is the person looking?

    public string timeStamp;

    public TrainingData(PoseInfo poseInformation, int targetRegion, int inferredRegion)
    {
        subjectID = poseInformation.subjectID;
        stimulusNum = poseInformation.stimulusNum;
        timeStamp = poseInformation.timeStamp;

        targetRegionNumber = targetRegion;

        lookingAtRegion = inferredRegion;
    }
}
