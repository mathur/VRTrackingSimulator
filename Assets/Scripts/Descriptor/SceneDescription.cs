﻿//Data structure for serializing pose information

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ObjectDescription
{
    public Vector3 position; //position of object
    //public Quaternion rotation = Quaternion.identity; //rotation of object
    public bool shape; //true for cube, false for sphere
    public short material; //colour 0- green 1-red 2-yellow 3-blue
    public Vector3 sphericalCoord;
    public ObjectDescription(Vector3 a, /*Quaternion b,*/ bool c, short d, Vector3 e)
    {
        position = a;
        //rotation = b;
        shape = c;
        material = d;
        sphericalCoord = e;
    }
    public ObjectDescription(ObjectDescription copy)
    {
        position = copy.position;
        //rotation = b;
        shape = copy.shape;
        material = copy.material;
        sphericalCoord = copy.sphericalCoord;
    }
}

[System.Serializable]
public class SceneDescription //wrapper for the scene
{
    public int stimulusNumber;
    public int numOfObjects; //num of objects in scene


    public int regionID_1 = -1; //RegionID of the first object (target if it exists)
    public int regionID_2 = -1; //Two region ids specifically for the opposing region experiment

    public bool twoTargets; //Are there two targets

    public string region1Colour;
    public string region2Colour;

    public bool targetExists; //whether there is a target
    public bool isFeature; //true- feature search, false- conjunctive search

    public List<ObjectDescription> objects; //list of all objects

    public SceneDescription(int counter, int a, bool b, bool c, int rid1, int rid2)
    {
        stimulusNumber = counter;
        numOfObjects = a;
        targetExists = b;
        isFeature = c;
        regionID_1 = rid1; //Set regionId
        regionID_2 = rid2;
    }
}
