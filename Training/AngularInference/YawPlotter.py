"""
Reads data from the 3D Tracking training file and plots graphs of track points.
Here, the rotation along y-axis (up) is considered
"""

from __future__ import print_function
import sys
import numpy as np
import matplotlib.pyplot as plt
import json
import io

try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Python 2.x fallback
    from argparse import Namespace


def read_file(f_name):
    with io.open(f_name, 'r', encoding='utf16') as track_file:
        track_list = []
        for line in track_file:
            # Deserialize individual objects into a list
            # print(json.loads(line, object_hook=lambda d: Namespace(**d)))
            track_list.append(json.loads(line, object_hook=lambda d: Namespace(**d)))
        print("Number of lines in the track file read is ", len(track_list))
        return track_list


def main():
    # Read the tracked data
    track_list = read_file("..\\..\\TrackingData\\" + sys.argv[1] + ".txt")
    y_angle = np.array([])
    # Set the y angles to a numpy array

    for i in range(int((len(track_list)-1)/20)):
        elem = track_list[i]
        y_angle = np.append(y_angle, (elem.headRotEuler.y - 90))

    #print(y_angle)

    axis_labels = np.arange(1, len(y_angle) + 1)
    #print(axis_labels)


    plt.plot(axis_labels, y_angle, 'ro')
    plt.show()


if __name__ == "__main__":
    main()
