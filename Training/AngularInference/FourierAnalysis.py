"""
Reads data from the 3D Tracking file and does a Fourier analysis of
component frequencies. Argument 1 is subject id, argument 2 is stimulus num
"""

from __future__ import print_function
import sys
import numpy as np
import matplotlib.pyplot as plt
import json
import io
import datetime
from scipy import fftpack

try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Python 2.x fallback
    from argparse import Namespace


def read_file(f_name):
    with io.open(f_name, 'r', encoding='utf16') as track_file:
        track_list = []
        for line in track_file:
            # Deserialize individual objects into a list
            # print(json.loads(line, object_hook=lambda d: Namespace(**d)))
            track_list.append(json.loads(line, object_hook=lambda d: Namespace(**d)))
        print("Number of lines in the track file read is ", len(track_list))
        return track_list


def runningMean(x, N):
    return np.convolve(x, np.ones((N,))/N)[(N-1):]

def main():
    # Read the tracked data
    track_list = read_file("..\\..\\TrackingData\\" + sys.argv[1] + ".txt")
    y_angle = np.array([])
    axis_labels = np.array([])
    # Set the y angles to a numpy array

    stimulus_num = int(sys.argv[2])
    samples_per_sec = 50
    for i in range((len(track_list)-1)):
        # For each element in track file
        elem = track_list[i]
        if elem.stimulusNum == stimulus_num:
            # If stimulus number is that of interest
            y_angle = np.append(y_angle, elem.headRotEuler.y)

    # Subtract with average to do away with mode at the centre
    y_angle -= np.average(y_angle)

    X = fftpack.fft(y_angle)
    freqs = fftpack.fftfreq(len(y_angle)) * samples_per_sec

    # Plot the Fourier decomposition
    plt.figure()
    fig, ax = plt.subplots()
    ax.stem(freqs, np.abs(X))
    ax.set_xlabel('Frequency in Hertz [Hz]')
    ax.set_ylabel('Frequency Domain (Spectrum) Magnitude')
    ax.set_xlim(-samples_per_sec/4, samples_per_sec/4)
    ax.set_ylim(-5, 5000)
    plt.savefig(sys.argv[1] + "_" + str(stimulus_num) + "_freq.png")

    axis_labels = np.arange(0, len(y_angle) * 0.02, 0.02)
    #plt.plot(axis_labels, y_angle, 'ro')


if __name__ == "__main__":
    main()
