# Introduction
This folder contains graphs plotting head tracking data (angles- 3 degrees of freedom).
Data is segmented into separate folders for each subject.
`All.png` represents all the tracking data for a particular subject.
`xxx.png` files represent tracking data for a particular subject for stimulus number `xxx`.
 
# Further operations
The graphs are segmented into `Left` and `Right` depending on the initial direction of turn chosen by the participant.
The initial direction is obtained using a gradient over the first 25 tracking values, which is ~0.5 second.
Note that some graphs may not look accurate at first glance because they only use data from the first 25 values, which may be a very short time horizon.
