"""
Reads data from the 3D Tracking training file and plots graphs of track points.
Here, the rotation along y-axis (up) is considered.
"""

from __future__ import print_function
import sys
import numpy as np
import matplotlib.pyplot as plt
import json
import io
import pathlib # For dir related functions

try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Python 2.x fallback
    from argparse import Namespace


def read_file(f_name):
    with io.open(f_name, 'r', encoding='utf16') as track_file:
        track_list = []
        for line in track_file:
            # Deserialize individual objects into a list
            # print(json.loads(line, object_hook=lambda d: Namespace(**d)))
            track_list.append(json.loads(line, object_hook=lambda d: Namespace(**d)))
        print("Number of lines in the track file read is ", len(track_list))
        return track_list

def open_file_and_plot(sid):
    # Read the tracked data
    track_list = read_file("..\\..\\TrackingData\\" + sid + ".txt")
    y_angle = np.array([])
    x_angle = np.array([])
    z_angle = np.array([])

    # We shall write graphs to a specific directory -> make sure it exists
    pathlib.Path(".\\RollPitchYawPlots\\" + sid).mkdir(exist_ok=True)

    # Read angles for each stimulus number
    for i in range(int((len(track_list) - 1))):
        elem = track_list[i]
        x_angle = np.append(x_angle, elem.headRotEuler.x)
        y_angle = np.append(y_angle, elem.headRotEuler.y)
        z_angle = np.append(z_angle, elem.headRotEuler.z)

    # print(y_angle)

    axis_labels = np.arange(1, len(y_angle) + 1)
    # print(axis_labels)
    plt.figure(figsize=(600, 15))
    # plt.plot(axis_labels, np.rad2deg(np.unwrap(np.deg2rad(x_angle))), 'ro', np.rad2deg(np.unwrap(np.deg2rad(y_angle))), 'go', np.rad2deg(np.unwrap(np.deg2rad(z_angle))), 'bo')

    plt.plot(axis_labels, x_angle, 'ro', y_angle, 'go', z_angle, 'bo')
    plt.legend(('x-axis', 'y-axis', 'z-axis'), loc='upper right')
    plt.savefig("RollPitchYawPlots\\" + sid + "\\All.png")
    # Clear all memory/flush
    plt.clf()
    plt.cla()
    plt.close()


def main():
    # All possible subjects
    arr_of_subjects = ["S016", "S017", "S019", "S020", "SB021", "S023"]
    for sid in arr_of_subjects:
        open_file_and_plot(sid)

if __name__ == "__main__":
    main()
