"""
Reads data from the 3D Tracking training file and plots graphs of track points, separated by subject id
and stimulus num. The program also demarcates graphs depending on gradient of first 25 data points.
"""

from __future__ import print_function
import sys
import numpy as np
import matplotlib.pyplot as plt
import json
import io
import pathlib  # For dir related functions

try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Python 2.x fallback
    from argparse import Namespace


def read_file(f_name):
    with io.open(f_name, 'r', encoding='utf16') as track_file:
        track_list = []
        for line in track_file:
            # Deserialize individual objects into a list
            # print(json.loads(line, object_hook=lambda d: Namespace(**d)))
            track_list.append(json.loads(line, object_hook=lambda d: Namespace(**d)))
        print("Number of lines in the track file read for ", f_name, "is ", len(track_list))
        return track_list


def first_turn_is_left(y_angle):
    # Use the first few Yaw values to obtain a guess for the first direction
    gradient = np.gradient(y_angle[:25])
    # print("The gradient is ", gradient)
    mean = np.mean(gradient)
    return np.sign(mean)

def open_file_and_plot(sid):
    # Read the tracked data
    track_list = read_file("..\\..\\TrackingData\\" + sid + ".txt")

    # We shall write graphs to a specific directory -> make sure it exists
    pathlib.Path(".\\RollPitchYawPlots\\" + sid).mkdir(exist_ok=True)
    pathlib.Path(".\\RollPitchYawPlots\\" + sid + "\\Left").mkdir(exist_ok=True)
    pathlib.Path(".\\RollPitchYawPlots\\" + sid + "\\Right").mkdir(exist_ok=True)

    y_angle = np.array([])
    x_angle = np.array([])
    z_angle = np.array([])

    # Separate stimulus numbers

    prev_stimulus_num = track_list[0].stimulusNum  # Initial value
    for i in range(int((len(track_list) - 1))):
        # Go through all data
        elem = track_list[i]
        curr_stimulus_num = track_list[i].stimulusNum
        # If a new stimulus number series is beginning
        if prev_stimulus_num != curr_stimulus_num:
            # print("Writing stimulus number ", prev_stimulus_num)
            # Save relevant graph
            axis_labels = np.arange(1, len(y_angle) + 1)
            plt.plot(axis_labels, x_angle, 'ro', y_angle, 'go', z_angle, 'bo')
            plt.legend(('x-axis', 'y-axis', 'z-axis'), loc='upper right')
            plt.savefig(".\\RollPitchYawPlots\\" + sid + "\\" + str(prev_stimulus_num) + ".png")

            # Check which side was the initial turn

            if len(y_angle) > 25:
                # If there are at least a few entries for this stimulus number
                res = first_turn_is_left(y_angle)
                if res == 1:
                    # One turn
                    plt.savefig(".\\RollPitchYawPlots\\" + sid + "\\Right\\" + str(prev_stimulus_num) + ".png")
                elif res == -1:
                    # The other direction
                    plt.savefig(".\\RollPitchYawPlots\\" + sid + "\\Left\\" + str(prev_stimulus_num) + ".png")
                else:
                    print("WARNING: Gradient was 0 for data-points of stimulus number ", prev_stimulus_num)
            else:
                print("WARNING: Too few data-points for stimulus number ", prev_stimulus_num)

            # Clear all memory/flush
            plt.clf()
            plt.cla()
            plt.close()
            # Reset/Set variables
            prev_stimulus_num = curr_stimulus_num
            y_angle = np.array([])
            x_angle = np.array([])
            z_angle = np.array([])

        x_angle = np.append(x_angle, elem.headRotEuler.x)
        y_angle = np.append(y_angle, elem.headRotEuler.y)
        z_angle = np.append(z_angle, elem.headRotEuler.z)


def main():
    # An array of subjects to get data from
    # ["S001", "S002", "S003", "S004", "S008", "S009", "S010", "S011", "S012", "S013"]
    arr_of_subjects = ["S016", "S017", "S019", "S020", "SB021", "S023"]
    for sid in arr_of_subjects:
        open_file_and_plot(sid)


if __name__ == "__main__":
    main()
