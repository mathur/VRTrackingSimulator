"""
Reads data from the 3D Tracking training file and plots graphs of track points.
Here, the rotation along y-axis (up) is considered
"""

from __future__ import print_function
import sys
import numpy as np
import json
import io

try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Python 2.x fallback
    from argparse import Namespace


def read_file(f_name):
    with io.open(f_name, 'r', encoding='utf16') as track_file:
        track_list = []
        for line in track_file:
            # Deserialize individual objects into a list
            # print(json.loads(line, object_hook=lambda d: Namespace(**d)))
            track_list.append(json.loads(line, object_hook=lambda d: Namespace(**d)))
        print("Number of lines in the track file read is ", len(track_list))
        return track_list


def main():
    # Read the tracked data
    track_list = read_file("..\\..\\TrackingData\\" + sys.argv[1] + ".txt")

    # Initialize very large arrays
    x_angle_movement = np.array([])
    y_angle_movement = np.array([])
    z_angle_movement = np.array([])

    # Temp variable for parsing list and getting within stimulus num differences
    prev_stimulus_num = 0
    prev_x = 0
    prev_y = 0
    prev_z = 0

    for elem in track_list:
        # For each element in the loaded list

        # If this element's stimulus number is of interest
        if prev_stimulus_num != elem.stimulusNum:
            # If a stimulus number series has begun
            prev_stimulus_num = elem.stimulusNum

            # Set values of previous angles
            prev_x = elem.headRotEuler.x
            prev_y = elem.headRotEuler.y
            prev_z = elem.headRotEuler.z

        # If this is a continuation in stimulus number
        elif prev_stimulus_num == elem.stimulusNum:

            # Set difference values
            diff_x = prev_x - elem.headRotEuler.x
            diff_y = prev_y - elem.headRotEuler.y
            diff_z = prev_z - elem.headRotEuler.z

            # Set values of previous angles
            prev_x = elem.headRotEuler.x
            prev_y = elem.headRotEuler.y
            prev_z = elem.headRotEuler.z

            x_angle_movement = np.append(x_angle_movement, diff_x)
            y_angle_movement = np.append(y_angle_movement, diff_y)
            z_angle_movement = np.append(z_angle_movement, diff_z)

    print("For x axis, net movement is ", np.linalg.norm(x_angle_movement))
    print("For y axis, net movement is ", np.linalg.norm(y_angle_movement))
    print("For z axis, net movement is ", np.linalg.norm(z_angle_movement))


if __name__ == "__main__":
    main()
