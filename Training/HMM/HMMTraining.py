"""
Reads data from the 3D Tracking training file so as to learn a HMM
"""

from __future__ import print_function
import numpy as np
from hmmlearn import hmm
import json

try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Python 2.x fallback
    from argparse import Namespace

	
def main():
	print("We are going to train a Hidden Markov Model using transitional tracking data.")
    file = open("..\\..\\TrainingData-transitional.txt", 'r')
    track_list = []
    for line in file:
        # deserialize individual objects into a list
        track_list.append(json.loads(line, object_hook=lambda d: Namespace(**d)))
    print(len(track_list), "objects were read.")

    # Select stimuli numbers with a specific region number
    stimuli_num = np.array([])
    for elem in track_list:
        if elem.targetRegionNumber == 5:
            stimuli_num = np.append(stimuli_num, elem.stimulusNum)
    stimuli_num = np.unique(stimuli_num)

    print("The chosen stimulus numbers are: ", stimuli_num)

    # Building the emission array

    elem_array = np.array([])
    for s_num in stimuli_num:
        # For all stimuli numbers in stimuli_num
        temp_array = np.array([])
        for elem in track_list:
            if elem.stimulusNum == s_num:
                temp_array = np.append(temp_array, elem.lookingAtRegion)
        if len(temp_array) < 3:  # If the transitions are very small in number
            continue
        # Pad array with zeros so as to vertically stack observations
        temp_array = np.pad(temp_array, (0, (10 - len(temp_array))), mode='constant')
        # If our emission array is empty, give it an initial value
        if len(elem_array) == 0:
            elem_array = temp_array
        else:
            elem_array = np.vstack((elem_array, temp_array))

    print("The chosen emission matrix is ", elem_array)

    # Set start probability
    start_prob = np.array([0.25, 0.25, 0.25, 0.25])
    start_prob = np.append(start_prob, np.zeros(28))

    # Set start transition probability
    trans_prob = np.zeros((32, 32))

    # Discrete emission HMM
    model = hmm.MultinomialHMM(n_components=32, startprob_prior=start_prob,
                               transmat_prior=trans_prob, n_iter=100)

    model.fit(elem_array)


if __name__ == "__main__":
    main()
