"""
The aim of this script is to quantify difference between two Markov Models by doing a frobenius norm
of the difference of the two models.
"""

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import json

try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Python 2.x fallback
    from argparse import Namespace


def diff():
    arr_of_subjects = ["S001", "S002", "S003", "S004", "S008", "S009", "S010", "S011", "S012", "S013"]

    output_file = open("Results\\NormOfDifferences.txt", "w")

    for id1 in range(len(arr_of_subjects)-1, 0, -1):
        for id2 in range(id1):
            theta_arr1 = np.load("Results\\theta_" + arr_of_subjects[id1] + ".npy")
            theta_arr2 = np.load("Results\\theta_" + arr_of_subjects[id2] + ".npy")
            # Difference of matrices theta
            theta_mat_diff = theta_arr1-theta_arr2
            np.savetxt("Results\\theta_dif_" + arr_of_subjects[id1] + "_" + arr_of_subjects[id2] + ".csv", theta_mat_diff)
            result_theta = np.linalg.norm(theta_mat_diff)

            pi_arr1 = np.load("Results\\pi_" + arr_of_subjects[id1] + ".npy")
            pi_arr2 = np.load("Results\\pi_" + arr_of_subjects[id2] + ".npy")
            # Difference of matrices pi
            pi_mat_diff = pi_arr1 - pi_arr2
            np.savetxt("Results\\pi_dif_" + arr_of_subjects[id1] + "_" + arr_of_subjects[id2] + ".csv", pi_mat_diff)
            result_pi = np.linalg.norm(pi_mat_diff)

            print(arr_of_subjects[id1] + " and " + arr_of_subjects[id2] + " is " + str(result_theta + result_pi))
             (arr_of_subjects[id1] + " and " + arr_of_subjects[id2] + " is " + str(result_theta + result_pi) + "\n")


if __name__ == "__main__":
    diff()
