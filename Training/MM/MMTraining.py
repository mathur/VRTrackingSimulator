"""
Reads data from the 3D Tracking training file so as to learn a Markov Model.
Argument is identifier of the subject (eg- S00X)
"""

from __future__ import print_function
import sys
import numpy as np
import matplotlib.pyplot as plt
import json

try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Python 2.x fallback
    from argparse import Namespace


def clean(data_list):
    cleaned_data_list = data_list
    # Fix the first few entries, which are usually erroneous
    prev_stimulus_num = 0
    # List of regions that are directly in front of user
    allowed_regions_for_initial = [2, 3, 30, 31]
    for id in range(len(cleaned_data_list)-1):
        # For each index in the list
        if prev_stimulus_num != cleaned_data_list[id].stimulusNum:
            # If a new stimulus number series is starting
            if cleaned_data_list[id+3].lookingAtRegion in allowed_regions_for_initial:
                cleaned_data_list[id].lookingAtRegion = cleaned_data_list[id+3].lookingAtRegion
                cleaned_data_list[id+1].lookingAtRegion = cleaned_data_list[id+3].lookingAtRegion
                cleaned_data_list[id+2].lookingAtRegion = cleaned_data_list[id+3].lookingAtRegion
            elif cleaned_data_list[id+4].lookingAtRegion in allowed_regions_for_initial:
                cleaned_data_list[id].lookingAtRegion = cleaned_data_list[id + 4].lookingAtRegion
                cleaned_data_list[id + 1].lookingAtRegion = cleaned_data_list[id + 4].lookingAtRegion
                cleaned_data_list[id + 2].lookingAtRegion = cleaned_data_list[id + 4].lookingAtRegion
                cleaned_data_list[id + 3].lookingAtRegion = cleaned_data_list[id + 4].lookingAtRegion
            elif cleaned_data_list[id+2].lookingAtRegion in allowed_regions_for_initial:
                cleaned_data_list[id].lookingAtRegion = cleaned_data_list[id+2].lookingAtRegion
                cleaned_data_list[id+1].lookingAtRegion = cleaned_data_list[id+2].lookingAtRegion
            else:
                print("Error! The cleaning of initial elements of each stimuli failed. For stimulus number ",
                      cleaned_data_list[id+3].stimulusNum, " subject ", cleaned_data_list[id+3].stimulusNum,
                      cleaned_data_list[id+3].subjectID)
            prev_stimulus_num = cleaned_data_list[id].stimulusNum

    print("Cleaning of training data done!")
    return cleaned_data_list


def read():
    path = "..\\..\\TrainingData\\" + sys.argv[1] + ".txt"
    file = open(path, 'r')
    track_list = []
    for line in file:
        # Deserialize individual objects into a list
        track_list.append(json.loads(line, object_hook=lambda d: Namespace(**d)))
    print(len(track_list), "objects were read.")
    return track_list


def main():
    print("We are going to train a Markov Model using temporal tracking data.")
    track_list = read()

    # Select stimuli numbers with a specific region number
    # stimuli_num = np.array([])
    # for elem in track_list:
    #     if elem.targetRegionNumber == 5:
    #         # We focus on region number 5
    #         stimuli_num = np.append(stimuli_num, elem.stimulusNum)
    # Retain only unique stimulus numbers
    # stimuli_num = np.unique(stimuli_num)
    # print("The chosen stimulus numbers are: ", stimuli_num)

    # Clean the data read
    track_list = clean(track_list)

    num_i_to_j = np.zeros((32, 32))
    num_i_to_star = np.zeros(32)

    prev_stimulus_num = 0
    prev_look_region = 0

    num_i_begin = np.zeros(32)

    for elem in track_list:
        # For each element in the loaded list
        # if elem.stimulusNum in stimuli_num:
        # If this element's stimulus number is of interest
        if prev_stimulus_num != elem.stimulusNum:
            # If a stimulus number series has begun
            prev_stimulus_num = elem.stimulusNum
            prev_look_region = elem.lookingAtRegion
            # Increment relevant index in pi_i
            num_i_begin[elem.lookingAtRegion-1] += 1
        else:
            # In the same series of stimulus number
            curr_look_region = elem.lookingAtRegion
            # increment element of array that says there was a transition from prev_look_region to curr_look_region
            num_i_to_j[prev_look_region-1, curr_look_region-1] += 1
            # increment element of array that says there was a transition from prev_look_region to x
            num_i_to_star[prev_look_region-1] += 1
            # Set prev_look_region to the currently looking at region
            prev_look_region = curr_look_region

    print("The array N(i->x) is ", num_i_to_star)

    total_n = np.sum(num_i_begin)
    print("Total number of stimuli ", total_n)

    pi_i = num_i_begin / total_n
    print("The array pi_i is ", pi_i)

    # Calculating theta-> the other matrix used to identify the Markov Model
    theta_i_to_j = np.zeros((32, 32))

    for (i, j), value in np.ndenumerate(num_i_to_j):
        # Set theta(i,j) as probability of transition from i->j
        if num_i_to_star[i] != 0:  # Avoid division by 0
            theta_i_to_j[i, j] = num_i_to_j[i, j] / num_i_to_star[i]

    np.savetxt("Results\\theta_" + sys.argv[1] + ".csv", theta_i_to_j)
    np.savetxt("Results\\pi_" + sys.argv[1] + ".csv", pi_i)
    np.save("Results\\theta_" + sys.argv[1], theta_i_to_j)
    np.save("Results\\pi_" + sys.argv[1], pi_i)
    # np.savetxt("sigma.csv", num_i_to_j)

    # Modify the transition matrix to be shown
    theta_i_to_j_vis = np.copy(theta_i_to_j)
    np.fill_diagonal(theta_i_to_j_vis, 0)
    # Make a plot of the matrix
    plt.matshow(theta_i_to_j_vis)
    plt.colorbar()
    plt.savefig("Results\\theta_" + sys.argv[1])


if __name__ == "__main__":
    main()
