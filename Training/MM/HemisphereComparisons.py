"""
Reads data from the 3D Tracking training file so as to learn a Markov Model.
Argument is  name of the file (eg- )
"""

from __future__ import print_function
import sys
import numpy as np
import matplotlib.pyplot as plt
import json
import MMTraining

try:
    from types import SimpleNamespace as Namespace
except ImportError:
    # Python 2.x fallback
    from argparse import Namespace


def main():
    list = MMTraining.read()
    list = MMTraining.clean(list)
    output_file = open("Results\\HemisphereComparison_" + sys.argv[1] + ".txt", "w")
    # Some lists containing region numbers belonging to that set
    above_regions = [*range(1, 17)]
    below_regions = [*range(17, 33)]

    left_regions = [6, 5, 9, 10, 2, 1, 13, 14, 30, 29, 17, 18, 26, 25, 21, 22]
    right_regions = [23, 24, 28, 27, 19, 20, 32, 31, 15, 16, 4, 3, 11, 12, 8, 7]

    # Counter to count times in each particular region
    counter = np.array(np.zeros(4))

    for elem in list:
        if elem.lookingAtRegion in above_regions:
            counter[0] += 1
        elif elem.lookingAtRegion in below_regions:
            counter[1] += 1

        if elem.lookingAtRegion in left_regions:
            counter[2] += 1
        elif elem.lookingAtRegion in right_regions:
            counter[3] += 1

    total = counter[0] + counter[1]

    print("% above is ", (counter[0]/total)*100)
    output_file.write("% above is " + str((counter[0]/total)*100) + "\n")
    print("% below is ", (counter[1]/total)*100)
    output_file.write("% below is " + str((counter[1] / total) * 100) + "\n")
    print("% left is ", (counter[2]/total)*100)
    output_file.write("% left is " + str((counter[2] / total) * 100) + "\n")
    print("% right is ", (counter[3]/total)*100)
    output_file.write("% right is " + str((counter[3] / total) * 100) + "\n")


if __name__ == "__main__":
    main()
